/*
  Multiple Serial test

  Receives from serial ports 1 & 2. Each connected to either the Tx or Rx signal within the the controller of a Chinese heater.

  Resultant (cominded and tagged) data sent out on serial port 0 (the default debug port)
  
  This example works only with boards with more than one serial port like Arduino Mega, Due, Zero etc.

  The circuit:
  - Controller Tx line connected to Serial 1 Rx input
  - Controller Rx line connected to Serial 2 Rx input
  - Serial Monitor open on Serial port 0

  created 23 Aug 2018
  by Ray Jones

  This example code is in the public domain.
*/


void setup() {
  // initialize both listening serial ports
  // 25000 baud, Tx and Rx channels of Chinese heater comms interface:
  Serial1.begin(25000);   // Tx data to heater, special baud rate for Chinese heater controllers
  Serial2.begin(25000);   // Rx data from heater, special baud rate for Chinese heater controllers
  // initilaise serial monitor on serial port 0
  Serial.begin(115200);
}

void loop() {
  
  static bool TxLine = false;
  static bool RxLine = false;
  static int count = 0;
  
  // read from port 1, the "Tx Data" (to heater), send to the serial monitor:
  if (Serial1.available()) {
    int inByte = Serial1.read(); // read hex byte
    if(!TxLine) {
      count = 0;
      Serial.write("\r\nTx ");    // insert header if data starts on Tx Line (ends previous line)
    }
    TxLine = true;
    RxLine = false;
    char str[16];
    sprintf(str, "%02X ", inByte);  // send ASCII formatted
    Serial.write(str);
    count++;
    if(count >= 24) {
      count = 0;
      TxLine = false;
    }
  }

  // read from port 2, the "Rx Data" (from heater), send to the serial monitor:
  if(Serial2.available()) {
    int inByte = Serial2.read();
    if(!RxLine) {
      Serial.write("   Rx ");  // insert header if data started on Rx line (continues previous line)
    }
    RxLine = true;
    TxLine = false;
    char str[16];
    sprintf(str, "%02X ", inByte);
    Serial.write(str);
  }
}
