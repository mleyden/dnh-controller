/*
  Chinese Heater Half Duplex Serial Data Collection Tool

  Connects to the blue wire of a Chinese heater, which is the half duplex serial link.
  Receives data from serial port 1. 
  
  Terminology: Tx is to the heater unit, Rx is from the heater unit.
  
  The binary data is received from the line.
  If it has been > 100ms since the last activity this indicates a new frame 
  sequence is starting, synchronise as such then count off the next 48 bytes 
  storing them in the Data array. 

  The "outer loop" then detects the count of 48 and packages the data to be sent 
  over Serial to the USB attached PC.

  Typical data frame timing on the blue wire is:
  __Tx_Rx____________________________Tx_Rx____________________________Tx_Rx___________
    
  Rx to next Tx delay is always > 100ms and is paced by the controller.
  The delay before seeing Rx data after Tx is usually much less than 10ms.
  **The heater only ever sends Rx data in response to a data frame from the controller**
  
  Resultant data is tagged and sent out on serial port 0 (the default debug port), 
  along with a timestamp for relative timing.
  
  This example works only with boards with more than one serial port like Arduino 
  Mega, Due, Zero etc.

  The circuit:
  - Blue wire connected to Serial 1 Rx input - preferably with series 680ohm resistor.
  - Serial logging software on Serial port 0 via USB link

  created 24 Aug 2018 by Ray Jones

  modified 25 Aug by Ray Jones
    - simplifed to read 48 bytes, synchronised by observing a long pause 
      between characters. The heater only sends when prompted.
      No longer need to discrimate which packet of data would be present.

  This example code is in the public domain.
*/

unsigned long lasttime;        // used to calculate inter character delay

void setup() 
{
  // initialize listening serial port
  // 25000 baud, Tx and Rx channels of Chinese heater comms interface:
  // Tx/Rx data to/from heater, special baud rate for Chinese heater controllers
  Serial1.begin(25000);   
  
  // initialise serial monitor on serial port 0
  Serial.begin(115200);
  
  // prepare for first long delay detection
  lasttime = millis();
}

void loop() 
{
  static byte Data[48];
  static bool RxActive = false;
  static int count = 0;
  
  // read from port 1, the "Tx Data" (to heater), send to the serial monitor:
  if (Serial1.available()) {
  
    // calc elapsed time since last rx�d byte to detect start of frame sequence
    unsigned long timenow = millis();
    unsigned long diff = timenow - lasttime;
    lasttime = timenow;
    
    if(diff > 100) {       // this indicates the start of a new frame sequence
      RxActive = true;
    }
    
    int inByte = Serial1.read(); // read hex byte
    if(RxActive) {
      Data[count++] = inByte;
      if(count == 48) {
        RxActive = false;
      }
    }
  }

  // dump to PC after capturing all 48 bytes in a frame session
  if(count == 48) {  // filled both frames � dump
  
    count = 0;
    char str[16];
    
    sprintf(str, "%08d  ", lasttime);
    Serial.print(str);                 // print timestamp
    for(int i=0; i<48; i++) {
    
      if(i == 0)
        Serial.print("Tx ");           // insert Tx marker on first pass
        
      if(i == 24)
        Serial.print("Rx ");           // insert Rx marker after first 24 bytes
        
      sprintf(str, "%02X ", Data[i]);  // make 2 dig hex values
      Serial.print(str);               // and print     
                          
    }
    Serial.println();                  // newline and done
    
  }  // count == 48
  
}  // loop
 
