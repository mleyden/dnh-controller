# dnh-controller

Arduino based controller for the Chinese Diesel Heaters and for, few of us, a way to learn new skills and experiment with unknown toys.


- Here an Arduino [library](https://github.com/vinmenn/Crc16) to calculate CRC16

- Here a [library](https://github.com/universam1/SoftwareUart) for HalfDuplex Communication
